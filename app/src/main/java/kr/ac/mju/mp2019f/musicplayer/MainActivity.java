package kr.ac.mju.mp2019f.musicplayer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button Gbt, Pbt;
    private TextView Gtext, Ptext;




    private final int REQ = 2;

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    int wp = 0;
    int glike = 0;
    int gplay = 0;
    int plike = 0;
    int pplay = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Gbt = findViewById(R.id.Gbt);
        Pbt = findViewById(R.id.Pbt);
        Gtext = findViewById(R.id.Gtext);
        Ptext = findViewById(R.id.Ptext);



        Gbt.setOnClickListener(this);
        Pbt.setOnClickListener(this);

        SharedPreferences sp =getSharedPreferences("sp", MODE_PRIVATE);
        SharedPreferences.Editor edit  = sp.edit();

        glike = sp.getInt("glike",0);
        gplay = sp.getInt("gplay",0);
        plike = sp.getInt("plike",0);
        pplay = sp.getInt("pplay",0);

        Gtext.setText("Play: "+gplay +"   Likes:  "+glike);
        Ptext.setText("Play: "+pplay +"   Likes:  "+plike);

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.Gbt:

                wp = 0;

                Intent intent1 = new Intent(getApplicationContext(), Second.class);

                intent1.putExtra("wp",wp);
                intent1.putExtra("gplay",gplay);
                intent1.putExtra("glike",glike);
                startActivityForResult(intent1,REQ);
                break;


            case R.id.Pbt:

                wp = 1;

                Intent intent2 = new Intent(getApplicationContext(), Second.class);

                intent2.putExtra("wp",wp);
                intent2.putExtra("pplay",pplay);
                intent2.putExtra("plike",plike);
                startActivityForResult(intent2,REQ);
                break;

        }
    }

    /**
     * Dispatch incoming result to the correct fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQ){
            if(resultCode == 0){
                int wp;
                wp = data.getIntExtra("wp",-1);
                if(wp == 0){
                    gplay = data.getIntExtra("play",-1);
                    glike = data.getIntExtra("like",-1);
                    Gtext.setText("Play: "+gplay +"   Likes:  "+glike);
                }
                if(wp == 1){
                    pplay = data.getIntExtra("play",-1);
                    plike = data.getIntExtra("like",-1);
                    Ptext.setText("Play: "+pplay +"   Likes:  "+plike);
                }

            }
        }
    }
}
