package kr.ac.mju.mp2019f.musicplayer;

import android.app.IntentService;
import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class MyIntentService extends IntentService {

    private MediaPlayer mp;

    public MyIntentService() {
        super("MyIntentService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            int wp = intent.getIntExtra("wp", 0);
            if (wp == 0) {
                mp = MediaPlayer.create(getApplicationContext(), R.raw.good);
            } else
                mp = MediaPlayer.create(getApplicationContext(), R.raw.mp);
            mp.setLooping(false);
            mp.start();
            try {
                Thread.sleep(mp.getDuration()*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (mp.isPlaying()==false)
                stopSelf();

        }
    }

    @Override
    public void onDestroy() {
        if(mp.isPlaying()==false){
            mp.stop();
            super.onDestroy();
        }
    }
}
