package kr.ac.mju.mp2019f.musicplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Second extends AppCompatActivity implements View.OnClickListener{

    private ImageView ImageView;
    private Button sBt, paBt, lbt;
    private TextView mText, nText, tText, lText;
    private SeekBar SeekBar;
    private MediaPlayer mp;
    private SimpleDateFormat df;

    private Intent ServiceIntent;



    int pcount= 0;
    int lcount = 0;
    int wp = 0;
    int playcheck = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        ImageView = findViewById(R.id.imageView);
        sBt = findViewById(R.id.Sbt);
        paBt = findViewById(R.id.Pabt);
        lbt = findViewById(R.id.Lbt);
        mText = findViewById(R.id.mtext);
        nText = findViewById(R.id.nText);
        tText = findViewById(R.id.tText);
        lText = findViewById(R.id.Ltext);
        SeekBar = findViewById(R.id.seekBar);


        Bundle extras = getIntent().getExtras();
        df = new SimpleDateFormat("mm:ss");
        final Handler mHand = new Handler();

        nText.setText(df.format(new Date(0)));

        sBt.setOnClickListener(this);
        paBt.setOnClickListener(this);
        lbt.setOnClickListener(this);

        SharedPreferences sp =getSharedPreferences("sp", MODE_PRIVATE);




        if(extras != null){

            wp = getIntent().getIntExtra("wp",0);

            if(wp == 0){

                ImageView.setImageResource(R.drawable.real);
                mText.setText("Artist: IU\nTitle: 좋은날");
                lcount = extras.getInt("glike");
                pcount = extras.getInt("gplay");
                lText.setText("Likes  :"+lcount);

                mp = MediaPlayer.create(getApplicationContext(), R.raw.good);
                SeekBar.setMax(mp.getDuration());

                mp.seekTo(sp.getInt("gtime",0));
                SeekBar.setProgress(mp.getCurrentPosition());
                nText.setText(df.format(SeekBar.getProgress()));


            }

            if(wp == 1){

                ImageView.setImageResource(R.drawable.palette);
                mText.setText("Artist: IU\nTitle: Palette");
                lcount = extras.getInt("plike");
                pcount = extras.getInt("pplay");
                lText.setText("Likes  :"+lcount);

                mp = MediaPlayer.create(getApplicationContext(), R.raw.mp);
                SeekBar.setMax(mp.getDuration());

                mp.seekTo(sp.getInt("ptime",0));
                SeekBar.setProgress(mp.getCurrentPosition());
                nText.setText(df.format(SeekBar.getProgress()));

            }

            tText.setText(df.format(mp.getDuration()));




            SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(android.widget.SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser && mp !=null){
                        mp.seekTo(progress);
                        nText.setText(df.format(new Date(progress)));
                    }
                }

                @Override
                public void onStartTrackingTouch(android.widget.SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(android.widget.SeekBar seekBar) {

                }
            });

            Second.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(mp != null && mp.isPlaying()){
                        SeekBar.setProgress(mp.getCurrentPosition());
                        nText.setText(df.format(new Date(SeekBar.getProgress())));
                        tText.setText(df.format(new Date(mp.getDuration() - SeekBar.getProgress())));
                    }
                    mHand.postDelayed(this,100);
                }
            });
        }





    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.Sbt:
                if(mp == null)
                    return;
                if(mp.isPlaying()){
                    //mp.pause();
                    sBt.setBackgroundResource(R.drawable.start);
                }
                else{
                   // mp.start();
                    sBt.setBackgroundResource(R.drawable.pause);
                    playcheck++;
                    if(playcheck == 1){
                        pcount +=1;
                    }
                    ServiceIntent = new Intent(this, MyIntentService.class);
                    ServiceIntent.putExtra("wp",wp);
                    startService(ServiceIntent);
                }
                break;

            case R.id.Pabt:
                if(mp == null)
                    return;

                    mp.pause();
                    sBt.setBackgroundResource(R.drawable.start);
                    mp.seekTo(0);
                    SeekBar.setProgress(0);
                    nText.setText(df.format(new Date(0)));

                break;

            case R.id.Lbt:
                lcount += 1;
                lText.setText("Likes  :"+lcount);
                break;
        }

    }



    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(ServiceIntent != null){
            stopService(ServiceIntent);
            ServiceIntent = null;
        }

    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {
        Intent returnIntent = getIntent();
        if(mp != null){
            mp.stop();
            mp.release();
            mp = null;
        }
        returnIntent.putExtra("wp",wp);
        returnIntent.putExtra("play",pcount);
        returnIntent.putExtra("like",lcount);

        SharedPreferences sp =getSharedPreferences("sp", MODE_PRIVATE);
        SharedPreferences.Editor edit  = sp.edit();
        if(wp == 0){
            edit.putInt("gplay",pcount);
            edit.putInt("glike",lcount);
            edit.putInt("gtime",SeekBar.getProgress());
        }
        if(wp == 1){
            edit.putInt("pplay",pcount);
            edit.putInt("plike",lcount);
            edit.putInt("ptime",SeekBar.getProgress());
        }
        edit.apply();

       setResult(0,returnIntent);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        SharedPreferences sp =getSharedPreferences("sp", MODE_PRIVATE);
        SharedPreferences.Editor edit  = sp.edit();

        if(wp == 0){
            edit.putInt("gplay",pcount);
            edit.putInt("glike",lcount);
            edit.putInt("gtime",SeekBar.getProgress());
        }
        if(wp == 1){
            edit.putInt("pplay",pcount);
            edit.putInt("plike",lcount);
            edit.putInt("ptime",SeekBar.getProgress());
        }
        edit.apply();
        super.onStop();
        if(ServiceIntent != null){
                stopService(ServiceIntent);
                ServiceIntent = null;
        }

    }

}
